---
title: "Terms of use for Replication Data for: Impact of scale of aggregation on associations of cardiovascular hospitalization and socio-economic disadvantage, 2017"
author: "Ivan C. Hanigan"
date: "7 November 2017"
output: html_document
---


This is an agreement (“Agreement”) between you the downloader (“Downloader”) and the owner of the materials (“User”) governing the use of the materials (“Materials”) to be downloaded.

I. Acceptance of this Agreement

By downloading or otherwise accessing the Materials, Downloader represents his/her acceptance of the terms of this Agreement.

II. Modification of this Agreement

Users may modify the terms of this Agreement at any time. However, any modifications to this Agreement will only be effective for downloads subsequent to such modification. No modifications will supersede any previous terms that were in effect at the time of the Downloader’s download.

III. Use of the Materials

Use of the Materials include but are not limited to viewing parts or the whole of the content included in the Materials; comparing data or content from the Materials with data or content in other Materials; verifying research results with the content included in the Materials; and extracting and/or appropriating any part of the content included in the Materials for use in other projects, publications, research, or other related work products.

A    Representations

In Use of the Materials, Downloader represents that:

1.    Downloader is not bound by any pre-existing legal obligations or other applicable laws that prevent Downloader from downloading or using the Materials;
2.    Downloader will not use the Materials in any way prohibited by applicable laws;
3.    Downloader has no knowledge of and will therefore not be responsible for any restrictions regarding the use of Materials beyond what is described in this Agreement; and
4.    Downloader has no knowledge of and will therefore not be responsible for any inaccuracies and any other such problems with regards to the content of the Materials and the accompanying citation information.

B. Restrictions In his/her Use of the Materials

B.1 Restrictions regarding further research

Ethical approval for this research should be cited as 1) ACT Health Ethics Protocol: ETH.11.14.310 and 2) the University of Canberra Ethics Protocol: 12-158.

In accessing the data the Users undertook a specific agreement with ACT Health which governs the access privileges, and all Downloaders need to abide by that agreement. Specifically, the legal document binding the data release agreement includes the following specific restrictions:

1.   Data provided by ACT Health in response to this request must be used only for the purposes outlined in the original request. If there is a change of purpose/scope, a new data request must be submitted prior to the recipient using the supplied data for the changed purpose or scope.
2.   The recipient must not grant any third party access to the data unless the third party has obtained written approval from ACT Health by submitting a 'Conditions for the Release of Data' document.
3.   If released data are to be included in any public document, a copy of the planned data release must be approved by ACT Health prior to the data release and acknowledgment of the source must be adequately noted.

Therefore any new use of these data must be assessed to ensure they follow the scope of the research questions outlined in the original approved project plan which accompanied the original request. For example this means that reproducing the analytic results would be allowed, and to a limited extent additional analyses could be conducted, but no new (additional) research questions could be investigated without first being assessed and approved by the relevant Human Research Ethics Committees.

What this means in practice is that a new analysis might be proposed which only seeks to explore the impact of re-parametrization of the spatial model we report, and such a request would clearly fall within the scope of our original proposed study protocol. In such a case the researcher requesting data would then only need to agree to adhere to the ACT Health department's data release rules, as well as the standard ethics guidelines for human research in Australia. However, if a hypothetical researcher requested access to these data in order to pursue a new research question that investigated (for example) the proportion of residents with a specific ethnic minority, then that would be outside the original research protocol and require a new ethics application to be reviewed.

These are important concerns in Australia. All human research in Australia is governed by the 'National Statement on Ethical Conduct in Human Research' Guidelines from the National Health and Medical Research Council Act 1992. The Introduction to this National Statement contains a broad definition of 'participants' and notes that the impact of research on wider populations is an important ethical consideration in the design, review and conduct of human research.

Therefore our dataset should not be made publicly available, but instead be published under the restrictions that the data will be made available on request, subject to the relevant approvals being gained.

To achieve the best outcome in terms of data preservation, we have published these data in the Australian Data Archives (ADA), where the metadata are discoverable via their data portal and the dataset has a DOI. To meet the requirement for accessibility, the requests that are made via the ADA data portal will then be forwarded by the User to the relevant contact at ACT Health if necessary.


B.2 Restrictions regarding identifiability

Downloaders cannot:

1.    obtain information from the Materials that results in Downloader or any third party(ies) directly or indirectly identifying any research subjects with the aid of other information acquired elsewhere;
2.    produce connections or links among the information included in User’s datasets (including information in the Materials), or between the information included in User’s datasets (including information in the Materials) and other third-party information that could be used to identify any individuals or organizations, not limited to research subjects; and
3.    extract information from the Materials that could aid Downloader in gaining knowledge about or obtaining any means of contacting any subjects already known to Downloader.

IV. Representations and Warranties

USER REPRESENTS THAT USER HAS ALL RIGHTS REQUIRED TO MAKE AVAILABLE AND DISTRIBUTE THE MATERIALS. EXCEPT FOR SUCH REPRESENTATION, THE MATERIALS IS PROVIDED “AS IS” AND “AS AVAILABLE” AND WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTIES IMPLIED BY ANY COURSE OF PERFORMANCE OR USAGE OF TRADE, ALL OF WHICH ARE EXPRESSLY DISCLAIMED.

WITHOUT LIMITING THE FOREGOING, USER DOES NOT WARRANT THAT: (A) THE MATERIALS ARE ACCURATE, COMPLETE, RELIABLE OR CORRECT; (B) THE MATERIALS FILES WILL BE SECURE ; (C) THE MATERIALS WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; (D) ANY DEFECTS OR ERRORS WILL BE CORRECTED; (E) THE MATERIALS AND ACCOMPANYING FILES ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; OR (F) THE RESULTS OF USING THE MATERIALS WILL MEET DOWNLOADER’S REQUIREMENTS. DOWNLOADER’S USE OF THE MATERIALS IS SOLELY AT DOWNLOADER’S OWN RISK.

V. Limitation of Liability

IN NO EVENT SHALL USER BE LIABLE UNDER CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE OR ANY OTHER LEGAL THEORY WITH RESPECT TO THE MATERIALS (I) FOR ANY DIRECT DAMAGES, OR (II) FOR ANY LOST PROFITS OR SPECIAL, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER.

VI. Indemnification

Downloader will indemnify and hold User harmless from and against any and all loss, cost, expense, liability, or damage, including, without limitation, all reasonable attorneys’ fees and court costs, arising from the i) Downloader’s misuse of the Materials; (ii) Downloader’s violation of the terms of this Agreement; or (iii) infringement by Downloader or any third party of any intellectual property or other right of any person or entity contained in the Materials. Such losses, costs, expenses, damages, or liabilities shall include, without limitation, all actual, general, special, and consequential damages.

VII. Dispute Resolution

Downloader and User agree that any cause of action arising out of or related to the download or use of the Materials must commence within one (1) year after the cause of action arose; otherwise, such cause of action is permanently barred.

This Agreement shall be governed by and interpreted in accordance with the laws of the Commonwealth of Australia (excluding the conflict of laws rules thereof). All disputes under this Agreement will be resolved in the applicable state or federal courts of Australia. Downloader consents to the jurisdiction of such courts and waives any jurisdictional or venue defenses otherwise available.

VIII. Integration and Severability

This Agreement represents the entire agreement between Downloader and User with respect to the downloading and use of the Materials, and supersedes all prior or contemporaneous communications and proposals (whether oral, written or electronic) between Downloader and User with respect to downloading or using the Materials. If any provision of this Agreement is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that the Agreement will otherwise remain in full force and effect and enforceable.

IX. Miscellaneous

User may assign, transfer or delegate any of its rights and obligations hereunder without consent. No agency, partnership, joint venture, or employment relationship is created as a result of the Agreement and neither party has any authority of any kind to bind the other in any respect outside of the terms described within this Agreement. In any action or proceeding to enforce rights under the Agreement, the prevailing party will be entitled to recover costs and attorneys’ fees.
