
##########################################################################
# SA1 level
projdir <- "~/projects/importance_of_scale/impact-of-scale-manuscript"
setwd(projdir)

source("report-main-PMI.R")
source("report-main-PCVD.R")

# load the spatial data
shp <- readOGR("data_derived", "study_region_sa1_v3_20171110")
# join the residuals to the spatial data
shp_sa1 <- shp
str(shp_sa1@data)
str(a_sa1)
a_sa1$residuals_eb_1<- residuals(fit_eb_1)
a_sa1$residuals_eb<- residuals(fit_eb)
ls()

shp_sa1@data$SA1_M <- as.numeric(as.character(shp_sa1@data$SA1_M))
shp_sa1@data <- left_join(shp_sa1@data, a_sa1, by = "SA1_M")

# remove the suburb 'Acton', as no SEIFA disadvantage score
shp_sa1 <- shp_sa1[-
                   which(as.character(shp_sa1@data$SA1_7) == "8104903")
                  ,]

# remove Hall as no neighbours
shp_sa1 <- shp_sa1[-
                   which(as.character(shp_sa1@data$SA1_7) == "8104301")
                  ,]

############################################################################
# qc_missing_residuals
# remove any with missing residuals (these did not have populations,
# or SEIFA)
shp_sa1v2 <- shp_sa1[
                   is.na(shp_sa1@data$residuals_eb_1)
   ,]
shp_sa1v2@data[,c("SA1_M", "SA1_R.x", "pop_pyl","disadvantage", "residuals_eb")]
summary(shp_sa1v2@data$residuals_eb_1)
str(shp_sa1v2@data[,1:10])
############################################################################

# remove any with missing residuals (these did not have populations,
# or SEIFA)
shp_sa1 <- shp_sa1[
                   !is.na(shp_sa1@data$residuals_eb_1)
   ,]
shp_sa1@data[,c("SA1_M", "SA1_R.x", "pop_pyl","disadvantage", "residuals_eb")]
summary(shp_sa1@data$residuals_eb_1)
str(shp_sa1@data[,1:10])

nrow(shp_sa1@data)
# 832
ncol(shp_sa1@data)
# 18
paste(names(shp_sa1@data), sep='', collapse = "', '")
namlist <- c('SA1_M', 'SA1_7', 'SA2_NAME11.x', 'SA1_R.x',  'pop_pyl', 'sir_pmi_ra',   'sir_pcvd_r', 'IRSD_Score', 'disadvantage',  'x', 'y', 'aged', 'medical', 'residuals_eb_1', 'residuals_eb')
all(namlist %in%
    names(shp_sa1@data)
    )
shp_sa1 <- shp_sa1[,namlist]
plot(shp_sa1, col = "grey")



#############################################    
### Just do the LISA maps
png("figures_and_tables/pcvd_LISAmap.png", width = 1250, height = 1000)
par(mfrow = c(1,2), cex = 1.5)
show_missing <- F
# model 1
# get inputs ready for the test-RSA-module
shp_soco <- shp_sa1
nbsa1 <- poly2nb(shp_soco, snap = 0, queen = T)
soco <- shp_sa1@data
soco$PPOV <- soco$residuals_eb_1
# to avoid the   ## Warning message:
  ## In nb2listw(nbsa1, glist = idw, style = "C", zero.policy = T) :
  ##   zero sum general weights

nbsa1.2 <- nb2listw(nbsa1, style = "C", zero.policy=T)  # no warning!
soco_nbq_w <- nbsa1.2
figtitle <- "PCVD Model 1 at SA1"
sa1 <- T
source("report-test-RSA-module.R")
#Otherwise you will get this Warning message:
#In sqrt(res[, 3]) : NaNs produced

# model 2

soco <- shp_sa1@data
soco$PPOV <- soco$residuals_eb
soco_nbq_w <- nbsa1.2
figtitle <- "PCVD Model 2 at SA1"
sa1 <- T
source("report-test-RSA-module.R")
dev.off()

##########################################################################
# SA2 level
projdir <- "~/projects/importance_of_scale/impact-of-scale-manuscript"
setwd(projdir)

source("report-main-PMI.R")
source("report-main-PCVD.R")

shp <- readOGR("data_derived", "study_region_sa2_v3_20171110")
plot(shp, col = "lightgrey")
shp_sa2 <- shp
ls()
str(shp_sa2@data)
str(a_sa2)

a_sa2$residuals_eb_1<- residuals(fit_eb2_1)
a_sa2$residuals_eb<- residuals(fit_eb2)


shp_sa2@data$SA2_MAIN11 <- as.numeric(as.character(shp_sa2@data$SA2_MAIN11))
shp_sa2@data$SA2_M <- as.numeric(as.character(shp_sa2@data$SA2_MAIN11))

shp_sa2@data <- left_join(shp_sa2@data, a_sa2, by = "SA2_MAIN11")
str(shp_sa2@data)

## remove Hall as no neighbours
shp_sa2 <- shp_sa2[-
                   which(as.character(shp_sa2@data$SA2_5DIG11.x) == "81043")
                  ,]
#plot(shp_sa2)
############################################################################
# qc_missing_residuals
# remove any with missing residuals (these did not have populations,
# or SEIFA)
shp_sa2v2 <- shp_sa2[
                   is.na(shp_sa2@data$residuals_eb_1)
   ,]
str(shp_sa2v2@data)
shp_sa2v2@data[,c("SA2_M", "SA2_NAME11.x", "pp_py","disadvantage", "residuals_eb")]
# Acton, the university
summary(shp_sa2v2@data$residuals_eb_1)
str(shp_sa2v2@data[,1:10])
## 1 area
############################################################################

# remove any with missing residuals (these did not have populations,
# or SEIFA)
shp_sa2 <- shp_sa2[
                   !is.na(shp_sa2@data$residuals_eb_1)
   ,]


nrow(shp_sa2@data)
# 86
ncol(shp_sa2@data)
# 17
paste(names(shp_sa2@data), sep='', collapse = "', '")
namlist <- c('SA2_M', 'SA2_NAME11.x', 'pp_py', 'sir_pmi_rate_per1000',   'sir_pcvd_rate_per1000', 'irsd_score', 'disadvantage',  'x', 'y', 'aged', 'medical', 'residuals_eb_1', 'residuals_eb')
all(
    namlist %in% names(shp_sa2@data)
)
shp_sa2 <- shp_sa2[,namlist]
plot(shp_sa2, col = 'grey')



#############################################    
### Just do the LISA maps
png("figures_and_tables/pcvd_LISAmap_sa2.png", width = 1250, height = 1000)
par(mfrow = c(1,2), cex = 1.5)
show_missing <- F
# model 1
# get inputs ready for the test-RSA-module
shp_soco <- shp_sa2
nbsa1 <- poly2nb(shp_soco, snap = 0, queen = T)
soco <- shp_sa2@data
soco$PPOV <- soco$residuals_eb_1
# to avoid the   ## Warning message:
  ## In nb2listw(nbsa1, glist = idw, style = "C", zero.policy = T) :
  ##   zero sum general weights

nbsa1.2 <- nb2listw(nbsa1, style = "C", zero.policy=T)  # no warning!
soco_nbq_w <- nbsa1.2
figtitle <- "PCVD Model 1 at SA2"
sa1 <- F
source("report-test-RSA-module.R")
#Otherwise you will get this Warning message:
#In sqrt(res[, 3]) : NaNs produced

# model 2

soco <- shp_sa2@data
soco$PPOV <- soco$residuals_eb
soco_nbq_w <- nbsa1.2
figtitle <- "PCVD Model 2 at SA2"
sa1 <- F
source("report-test-RSA-module.R")
dev.off()
