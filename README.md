Supporting Information document for the article: `Impact of scale of aggregation on associations of cardiovascular disease and disadvantage'
---

Authors:  

- name: Ivan C. Hanigan, email: ivan.hanigan@canberra.edu.au  
- name: Thomas Cochrane
- name: Rachel Davey

Affiliation:

- affil1: Spatial Epidemiology Group, University of Canberra, Canberra, Australia
- affil2: University of Sydney, Sydney, Australia
